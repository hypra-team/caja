Patch upstreamed in 1.23, see https://github.com/mate-desktop/caja/pull/1254

diff --git a/data/Makefile.am b/data/Makefile.am
index 757958b..0bd161f 100644
--- a/data/Makefile.am
+++ b/data/Makefile.am
@@ -34,7 +34,12 @@ cajadata_DATA = \
 	caja-extras.placeholder  \
 	caja-suggested.placeholder \
 	caja.css \
-    caja-desktop.css \
+	caja-desktop.css \
+	a11y-caja-desktop-base.css \
+	caja-desktop-ContrastHigh.css \
+	caja-desktop-ContrastHighInverse.css \
+	caja-desktop-HighContrast.css \
+	caja-desktop-HighContrastInverse.css \
 	$(NULL)
 
 # app data file
diff --git a/data/a11y-caja-desktop-base.css b/data/a11y-caja-desktop-base.css
new file mode 100644
index 0000000..62721af
--- /dev/null
+++ b/data/a11y-caja-desktop-base.css
@@ -0,0 +1,18 @@
+/* base rules for highly contrasted desktop items */
+
+.caja-desktop.caja-canvas-item {
+  text-shadow: none;
+}
+
+.caja-desktop.caja-canvas-item {
+  color: @theme_fg_color;
+  background: @theme_bg_color;
+}
+
+.caja-desktop.caja-canvas-item:selected {
+  color: @theme_selected_fg_color;
+  background: @theme_selected_bg_color;
+}
+.caja-desktop.caja-canvas-item:selected:backdrop {
+  background: mix(@theme_selected_bg_color, @theme_selected_fg_color, 0.3);
+}
diff --git a/data/caja-desktop-ContrastHigh.css b/data/caja-desktop-ContrastHigh.css
new file mode 100644
index 0000000..a826ef8
--- /dev/null
+++ b/data/caja-desktop-ContrastHigh.css
@@ -0,0 +1 @@
+@import url("a11y-caja-desktop-base.css");
diff --git a/data/caja-desktop-ContrastHighInverse.css b/data/caja-desktop-ContrastHighInverse.css
new file mode 100644
index 0000000..a826ef8
--- /dev/null
+++ b/data/caja-desktop-ContrastHighInverse.css
@@ -0,0 +1 @@
+@import url("a11y-caja-desktop-base.css");
diff --git a/data/caja-desktop-HighContrast.css b/data/caja-desktop-HighContrast.css
new file mode 100644
index 0000000..a826ef8
--- /dev/null
+++ b/data/caja-desktop-HighContrast.css
@@ -0,0 +1 @@
+@import url("a11y-caja-desktop-base.css");
diff --git a/data/caja-desktop-HighContrastInverse.css b/data/caja-desktop-HighContrastInverse.css
new file mode 100644
index 0000000..a826ef8
--- /dev/null
+++ b/data/caja-desktop-HighContrastInverse.css
@@ -0,0 +1 @@
+@import url("a11y-caja-desktop-base.css");
diff --git a/src/caja-application.c b/src/caja-application.c
index 723edfc..e6995b6 100644
--- a/src/caja-application.c
+++ b/src/caja-application.c
@@ -2184,43 +2184,73 @@ caja_application_local_command_line (GApplication *application,
     return TRUE;    
 }
 
-
 static void
-init_icons_and_styles (void)
+load_custom_css (GtkCssProvider *provider,
+                 const gchar *filename,
+                 guint priority)
 {
-    GtkCssProvider *provider;
     GError *error = NULL;
+    gchar *path = g_build_filename (CAJA_DATADIR, filename, NULL);
 
-    /* add our custom CSS provider */
-    provider = gtk_css_provider_new ();
-    gtk_css_provider_load_from_path (provider,
-				CAJA_DATADIR G_DIR_SEPARATOR_S "caja.css", &error);
+    if (provider)
+        g_object_ref (provider);
+    else
+        provider = gtk_css_provider_new ();
+
+    gtk_css_provider_load_from_path (provider, path, &error);
 
     if (error != NULL) {
-        g_warning ("Can't parse Caja' CSS custom description: %s\n", error->message);
+        g_warning ("Can't parse Caja' CSS custom description '%s': %s\n",
+                   filename, error->message);
         g_error_free (error);
     } else {
         gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
-                               GTK_STYLE_PROVIDER (provider),
-                               GTK_STYLE_PROVIDER_PRIORITY_THEME);
+                                                   GTK_STYLE_PROVIDER (provider),
+                                                   priority);
     }
 
-/* add our desktop CSS provider,  ensures the desktop background does not get covered */
-    provider = gtk_css_provider_new ();
+    g_object_unref (provider);
+    g_free (path);
+}
 
-    gtk_css_provider_load_from_path (provider, CAJA_DATADIR G_DIR_SEPARATOR_S "caja-desktop.css", &error);
+static void
+reload_theme_css (GtkSettings    *settings,
+                  GParamSpec     *unused G_GNUC_UNUSED,
+                  GtkCssProvider *provider)
+{
+    gchar *theme_name;
+    gchar *css_theme_name;
+    gchar *path;
 
-    if (error != NULL) {
-        g_warning ("Can't parse Caja' CSS custom description: %s\n", error->message);
-        g_error_free (error);
-    } else {
-        gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
-                               GTK_STYLE_PROVIDER (provider),
-                               GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
-    }
+    g_object_get (settings, "gtk-theme-name", &theme_name, NULL);
+    css_theme_name = g_strconcat ("caja-desktop-", theme_name, ".css", NULL);
+    path = g_build_filename (CAJA_DATADIR, css_theme_name, NULL);
 
+    if (g_file_test (path, G_FILE_TEST_EXISTS))
+        load_custom_css (provider, css_theme_name, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
+    else /* just empty the provider */
+        gtk_css_provider_load_from_data (provider, "", 0, NULL);
 
-    g_object_unref (provider);
+    g_free (path);
+    g_free (css_theme_name);
+    g_free (theme_name);
+}
+
+static void
+init_icons_and_styles (void)
+{
+    GtkSettings *settings = gtk_settings_get_default ();
+    GtkCssProvider *provider;
+
+    /* add our custom CSS provider */
+    load_custom_css (NULL, "caja.css", GTK_STYLE_PROVIDER_PRIORITY_THEME);
+    /* add our desktop CSS provider,  ensures the desktop background does not get covered */
+    load_custom_css (NULL, "caja-desktop.css", GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
+    /* add theme-specific desktop CSS */
+    provider = gtk_css_provider_new ();
+    reload_theme_css (settings, NULL, provider);
+    g_signal_connect_data (settings, "notify::gtk-theme-name", G_CALLBACK (reload_theme_css),
+                           provider, (GClosureNotify) g_object_unref, 0);
 
     /* initialize search path for custom icons */
     gtk_icon_theme_append_search_path (gtk_icon_theme_get_default (),
